import { BoekeriePage } from './app.po';

describe('boekerie App', () => {
  let page: BoekeriePage;

  beforeEach(() => {
    page = new BoekeriePage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
