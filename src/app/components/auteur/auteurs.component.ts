import { AuteursService } from './../../services/auteurs.service';
import { Auteur } from './../../models/auteur';
import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-auteurs',
  templateUrl: './auteurs.component.html',
  styleUrls: ['./auteurs.component.css']
})
export class AuteursComponent implements OnInit {

  auteurs: Auteur[];

  constructor(private auteursService: AuteursService) { }

  ngOnInit() {
    this.getAuteurs();
  }

  getAuteurs() {
    this.auteursService.getAuteurs()
      .subscribe(
      auteurs => {
        this.auteurs = auteurs;
      });
  }

  deleteAuteur(id: number) {
    this.auteursService.deleteAuteur(id)
      .subscribe(res => {
        this.getAuteurs();
      });
  }
}