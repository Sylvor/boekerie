import { ActivatedRoute, Router } from '@angular/router';
import { GenresService } from './../../services/genres.service';
import { Genre } from './../../models/genre';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-genre-add',
  templateUrl: './genre-add.component.html',
  styleUrls: ['./genre-add.component.css']
})
export class GenreAddComponent implements OnInit {

  genre: Genre;

  constructor(private genresService: GenresService, private _router: Router) { }

  ngOnInit() {
  }

  addGenre(naam: string) {
    this.genresService.addGenre(naam).subscribe(res => {
      this._router.navigate(['/genres'])
    });
  }
}
