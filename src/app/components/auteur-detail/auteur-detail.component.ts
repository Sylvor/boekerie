import { Auteur } from './../../models/auteur';
import { ActivatedRoute } from '@angular/router';
import { AuteursService } from './../../services/auteurs.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-auteur-detail',
  templateUrl: './auteur-detail.component.html',
  styleUrls: ['./auteur-detail.component.css']
})
export class AuteurDetailComponent implements OnInit {

  auteur: Auteur;

  constructor(private auteursService: AuteursService, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    let id = this.activatedRoute.snapshot.params['id'];
    this.getAuteur(id);
  }

  getAuteur(id: number) {
    this.auteursService.getAuteur(id)
      .subscribe(
      auteur => {
        this.auteur = auteur;
      });
  }

}
