import { ActivatedRoute, Router } from '@angular/router';
import { AuteursService } from './../../services/auteurs.service';
import { Auteur } from './../../models/auteur';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-auteur-edit',
  templateUrl: './auteur-edit.component.html',
  styleUrls: ['./auteur-edit.component.css']
})
export class AuteurEditComponent implements OnInit {

  auteur: Auteur;

  constructor(private auteursService: AuteursService, private activatedRoute: ActivatedRoute, private _router: Router) { }

  ngOnInit() {
    let id = this.activatedRoute.snapshot.params['id'];
    this.getAuteur(id);
  }

  getAuteur(id: number) {
    this.auteursService.getAuteur(id)
      .subscribe(
      auteur => {
        this.auteur = auteur;
      });
  }

  editAuteur(naam: string) {
    this.auteur.naam = naam;
    this.auteursService.editAuteur(this.auteur).subscribe(res => {
      this._router.navigate(['/auteurs'])
    });
  }

}
