import { Router, ActivatedRoute } from '@angular/router';
import { GenresService } from './../../services/genres.service';
import { Genre } from './../../models/genre';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-genre-edit',
  templateUrl: './genre-edit.component.html',
  styleUrls: ['./genre-edit.component.css']
})
export class GenreEditComponent implements OnInit {

  genre: Genre;

  constructor(private genresService: GenresService, private activatedRoute: ActivatedRoute, private _router: Router) { }

  ngOnInit() {
    let id = this.activatedRoute.snapshot.params['id'];
    this.getGenre(id);
  }

  getGenre(id: number) {
    this.genresService.getGenre(id)
      .subscribe(
      genre => {
        this.genre = genre;
      });
  }

  editGenre(naam: string) {
    this.genre.naam = naam;
    this.genresService.editGenre(this.genre).subscribe(res => {
      this._router.navigate(['/genres'])
    });
  }

}
