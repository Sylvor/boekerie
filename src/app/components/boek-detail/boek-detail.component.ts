import { Boek } from './../../models/boek';
import { BoekenService } from './../../services/boeken.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-boek-detail',
  templateUrl: './boek-detail.component.html',
  styleUrls: ['./boek-detail.component.css']
})
export class BoekDetailComponent implements OnInit {

  boek: Boek;

  constructor(private boekenService: BoekenService, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    let isbn = this.activatedRoute.snapshot.params['isbn'];
    this.getBoek(isbn);
  }

  getBoek(isbn: string) {
    this.boekenService.getBoek(isbn)
      .subscribe(
      boek => {
        this.boek = boek;
      });
  }

}
