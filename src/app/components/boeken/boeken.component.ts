import { BoekenService } from './../../services/boeken.service';
import { Boek } from './../../models/boek';
import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-boeken',
  templateUrl: './boeken.component.html',
  styleUrls: ['./boeken.component.css']
})
export class BoekenComponent implements OnInit {

  boeken: Boek[];

  constructor(private boekenService: BoekenService) { }

  ngOnInit() {
    this.getBoeken();
  }

  getBoeken() {
    this.boekenService.getBoeken()
      .subscribe(
      boeken => {
        this.boeken = boeken;
      });
  }
  
  deleteBoek(isbn: string) {
    this.boekenService.deleteBoek(isbn)
      .subscribe(res => {
        this.getBoeken();
      });
  }
}