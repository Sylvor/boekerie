import { Router } from '@angular/router';
import { Genre } from './../../models/genre';
import { GenresService } from './../../services/genres.service';
import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-genres',
  templateUrl: './genres.component.html',
  styleUrls: ['./genres.component.css']
})
export class GenresComponent implements OnInit {

  genres: Genre[];

  constructor(private genresService: GenresService, private _router: Router) { }

  ngOnInit() {
    this.getGenres();
  }

  getGenres() {
    this.genresService.getGenres()
      .subscribe(
      genres => {
        this.genres = genres;
      });
  }

  deleteGenre(id: number) {
    this.genresService.deleteGenre(id)
      .subscribe(res => {
        this.getGenres();
      });
  }
}