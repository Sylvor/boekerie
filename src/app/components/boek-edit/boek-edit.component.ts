import { AuteursService } from './../../services/auteurs.service';
import { GenresService } from './../../services/genres.service';
import { Auteur } from './../../models/auteur';
import { Genre } from './../../models/genre';
import { ActivatedRoute, Router } from '@angular/router';
import { BoekenService } from './../../services/boeken.service';
import { Boek } from './../../models/boek';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-boek-edit',
  templateUrl: './boek-edit.component.html',
  styleUrls: ['./boek-edit.component.css']
})
export class BoekEditComponent implements OnInit {

  boek: Boek;
  genres: Genre[];
  auteurs: Auteur[];
  submitted: boolean;
  boekToevoegen: boolean = true;

  constructor(private boekenService: BoekenService, private genresService: GenresService, private auteursService: AuteursService,
    private activatedRoute: ActivatedRoute, private _router: Router) { }

  ngOnInit() {
    this.boek = new Boek();
    let isbn = this.activatedRoute.snapshot.params['isbn'];
    if (isbn != undefined) {
      this.boekToevoegen = false;
      this.getBoek(isbn);
    }
    this.genresService.getGenres().subscribe(
      genres => {
        this.genres = genres;
      });
    this.auteursService.getAuteurs().subscribe(
      auteurs => {
        this.auteurs = auteurs;
      });
    this.submitted = false;
  }

  getBoek(isbn: string) {
    this.boekenService.getBoek(isbn)
      .subscribe(
      boek => {
        this.boek = boek;
      });
  }

  addBoek(boek: Boek) {
    this.boekenService.addBoek(boek).subscribe(res => {
      this._router.navigate(['/boeken']);
    });
  }

  editBoek(boek: Boek) {
    this.boekenService.editBoek(this.boek).subscribe(res => {
      this._router.navigate(['/boeken']);
    });
  }

  addTag(tag: string) {
    this.boek.tags.push(tag);
  }

  removeTag(tag: string) {
    var index = this.boek.tags.indexOf(tag);
    if (index > -1) {
      this.boek.tags.splice(index, 1);
    }
  }

  onSubmit() {
    if (this.boekToevoegen) {
      this.addBoek(this.boek);
    } else {
      this.editBoek(this.boek);
    }
    this.submitted = true;
  }
}
