import { Router } from '@angular/router';
import { AuteursService } from './../../services/auteurs.service';
import { Auteur } from './../../models/auteur';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-auteur-add',
  templateUrl: './auteur-add.component.html',
  styleUrls: ['./auteur-add.component.css']
})
export class AuteurAddComponent implements OnInit {

  auteur: Auteur;

  constructor(private auteursService: AuteursService, private _router: Router) { }

  ngOnInit() {
  }

  addAuteur(naam: string) {
    this.auteursService.addAuteur(naam).subscribe(res => {
      this._router.navigate(['/auteurs'])
    });
  }
}
