import { AuteursService } from './services/auteurs.service';
import { GenresService } from './services/genres.service';
import { BoekenService } from './services/boeken.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { HttpModule, JsonpModule } from '@angular/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppComponent } from './app.component';
import { GenresComponent } from './components/genres/genres.component';
import { BoekenComponent } from './components/boeken/boeken.component';
import { HeaderComponent } from './components/header/header.component';
import { NavComponent } from './components/nav/nav.component';
import { AuteursComponent } from './components/auteur/auteurs.component';
import { BoekDetailComponent } from './components/boek-detail/boek-detail.component';
import { GenreEditComponent } from './components/genre-edit/genre-edit.component';
import { GenreAddComponent } from './components/genre-add/genre-add.component';
import { AuteurAddComponent } from './components/auteur-add/auteur-add.component';
import { AuteurEditComponent } from './components/auteur-edit/auteur-edit.component';
import { BoekEditComponent } from './components/boek-edit/boek-edit.component';

const appRoutes: Routes = [
  { path: 'boeken', component: BoekenComponent },
  { path: 'auteurs', component: AuteursComponent },
  { path: 'genres', component: GenresComponent },
  { path: 'boeken/detail/:isbn', component: BoekDetailComponent },
  { path: 'boeken/:isbn/edit', component: BoekEditComponent },
  { path: 'auteurs/:id/edit', component: AuteurEditComponent },
  { path: 'genres/:id/edit', component: GenreEditComponent },
  { path: 'boeken/add', component: BoekEditComponent },
  { path: 'auteurs/add', component: AuteurAddComponent },
  { path: 'genres/add', component: GenreAddComponent }
];


@NgModule({
  declarations: [
    AppComponent,
    GenresComponent,
    BoekenComponent,
    HeaderComponent,
    NavComponent,
    AuteursComponent,
    BoekDetailComponent,
    GenreEditComponent,
    GenreAddComponent,
    AuteurAddComponent,
    AuteurEditComponent,
    BoekEditComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    JsonpModule,
    RouterModule.forRoot(appRoutes),
    NgbModule.forRoot()
  ],
  providers: [BoekenService, GenresService, AuteursService],
  bootstrap: [AppComponent]
})
export class AppModule { }
