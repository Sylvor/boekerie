import { Genre } from './../models/genre';
import { Injectable } from '@angular/core';
import { Jsonp, URLSearchParams } from '@angular/http';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';

import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

@Injectable()
export class GenresService {
  private genresUrl = 'http://apis.dirkandries.be/api/genres';

  constructor(private http: Http) { }

  getGenres(): Observable<Genre[]> {
    return this.http.get(this.genresUrl)
      .map(this.extractGenres);
  }

  getGenre(id: number): Observable<Genre> {
    return this.http.get(this.genresUrl + '/' + id)
      .map(this.extractGenre);
  }

  editGenre(genre: Genre) {
    return this.http.put(this.genresUrl + '/' + genre.id, genre);
  }

  addGenre(naam: string) {
    return this.http.post(this.genresUrl, { naam });
  }

  deleteGenre(id: number) {
    return this.http.delete(this.genresUrl + '/' + id);
  }

  private extractGenres(res: Response) {
    let body = res.json();
    return body.value || {};
  }

  private extractGenre(res: Response) {
    let body = res.json();
    return body || {};
  }
}