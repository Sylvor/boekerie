import { Auteur } from './../models/auteur';
import { Injectable } from '@angular/core';
import { Jsonp, URLSearchParams } from '@angular/http';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';

import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

@Injectable()
export class AuteursService {
  private auteursUrl = 'http://apis.dirkandries.be/api/auteurs';

  constructor(private http: Http) { }

  getAuteurs(): Observable<Auteur[]> {
    return this.http.get(this.auteursUrl)
      .map(this.extractAuteurs);
  }

  getAuteur(id: number): Observable<Auteur> {
    return this.http.get(this.auteursUrl + '/' + id)
      .map(this.extractAuteur);
  }

  editAuteur(auteur: Auteur) {
    return this.http.put(this.auteursUrl + '/' + auteur.id, auteur);
  }

  addAuteur(naam: string) {
    return this.http.post(this.auteursUrl, { naam });
  }

  deleteAuteur(id: number) {
    return this.http.delete(this.auteursUrl + '/' + id);
  }

  private extractAuteurs(res: Response) {
    let body = res.json();
    return body.value || {};
  }

  private extractAuteur(res: Response) {
    let body = res.json();
    return body || {};
  }
}