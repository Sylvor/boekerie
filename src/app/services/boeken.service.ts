import { Boek } from './../models/boek';
import { Injectable } from '@angular/core';
import { Jsonp, URLSearchParams } from '@angular/http';
import { Http, Response, RequestOptions, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';

import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

@Injectable()
export class BoekenService {
  private boekenUrl = 'http://apis.dirkandries.be/api/boeken';

  constructor(private http: Http) { }

  getBoeken(): Observable<Boek[]> {
    return this.http.get(this.boekenUrl)
      .map(this.extractBoeken);
  }

  getBoek(isbn: string): Observable<Boek> {
    return this.http.get(this.boekenUrl + '/' + isbn)
      .map(this.extractBoek);
  }

  editBoek(boek: Boek) {
    console.log(boek);
    return this.http.put(this.boekenUrl + '/' + boek.isbn, boek);
  }

  addBoek(boek: Boek) {
    let headers = new Headers({ 'Content-Type': 'application/json' });

    return this.http.post(this.boekenUrl, JSON.stringify(boek), { headers: headers });
  }

  deleteBoek(isbn: string) {
    return this.http.delete(this.boekenUrl + '/' + isbn);
  }

  private extractBoeken(res: Response) {
    let body = res.json();
    return body.value || {};
  }

  private extractBoek(res: Response) {
    let body = res.json();
    return body || {};
  }
}