import { Auteur } from './auteur';
import { Genre } from './genre';

export class Boek {
    isbn: string;
    titel: string;
    genre: Genre;
    auteur: Auteur;
    cover?: string;
    uitgegeven?: number;
    prijs?: number;
    tags?: string[];
    sterren?: number;

    constructor() {
        this.genre = new Genre();
        this.auteur = new Auteur();
        this.tags = [];
    }
}